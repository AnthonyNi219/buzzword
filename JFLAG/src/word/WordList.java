package word;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

/**
 * Created by Anthony on 12/7/2016.
 */
public class WordList extends ArrayList {

    public boolean containsWord(String user) {
        for (int i = 0; i < size(); i++) {
            Word word = (Word) get(i);
            if (user.equals(word.getWord())) {
                return true;
            }
        }
        return false;
    }

    public int getTotalScoreInList() {
        int total = 0;
        for (int i = 0; i < size(); i++) {
            Word word = (Word) get(i);
            total += word.getScore();
        }
        return total;
    }

    public Word getWord(String word) {
        for (int i = 0; i < size(); i++) {
            Word data = (Word) get(i);
            if (word.equals(data.getWord()))
                return data;
        }
        return null;
    }

    @Override
    public boolean add(Object e) {
        Word wordData = (Word) e;
        String word = wordData.getWord();
        boolean isDupe = containsWord(word);
        if (!isDupe)
            super.add(wordData);
        return !isDupe;
    }

    @Override
    public void add(int index, Object e) {
        Word wordData = (Word) e;
        String word = wordData.getWord();
        boolean isDupe = containsWord(word);
        if (!isDupe)
            super.add(index, wordData);
    }

    public void sortByScore() {
        Collections.sort(this, new Comparator<Word>() {
            @Override
            public int compare(Word word1, Word word2) {
                int score1 = word1.getScore();
                int score2 = word2.getScore();

                return score1-score2;
            }
        });
    }
}
