package gui;

import apptemplate.AppTemplate;
import components.AppWorkspaceComponent;
import controller.BuzzWordController;
import javafx.event.EventHandler;
import javafx.geometry.Orientation;
import javafx.geometry.Pos;
import javafx.scene.control.*;
import javafx.scene.layout.*;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;
import propertymanager.PropertyManager;
import ui.AppGUI;

import java.io.IOException;

import static buzzword.BuzzWordProperties.*;
import static settings.AppPropertyType.*;


/**
 * This class serves as the GUI component for the Hangman game.
 *
 * @author Ritwik Banerjee, Anthony Ni
 */
public class Workspace extends AppWorkspaceComponent {

    AppTemplate app; // the actual application
    AppGUI      gui; // the GUI inside which the application sits

    Label               guiHeadingLabel;   // workspace (GUI) heading label
    BorderPane          mainPane;         // main pane for all the other panes, workspace
    VBox                topPane;          // top pane
    VBox                leftPane;         // left pane
    VBox                rightPane;        // right pane
    BorderPane          centerPane;       // center pane
    VBox                bottomPane;
    Button              createProfile;
    Button              login;
    Button              exit;
    BuzzWordController  controller;

    /**
     * Constructor for initializing the workspace, note that this constructor
     * will fully setup the workspace user interface for use.
     *
     * @param initApp The application this workspace is part of.
     * @throws IOException Thrown should there be an error loading application
     *                     data for setting up the user interface.
     */
    public Workspace(AppTemplate initApp) throws IOException {
        app = initApp;
        gui = app.getGUI();
        controller = (BuzzWordController) gui.getFileController();    //new BuzzWordController(app, startGame);
        initExitButton();
        layoutGUI();     // initialize all the workspace (GUI) components including the containers and their layout
        initHomeScreen();
        activateWorkspace(gui.getAppPane()); //activates workspace if not activated already
    }

    private void layoutGUI() {
        PropertyManager propertyManager = PropertyManager.getManager();
        guiHeadingLabel = new Label(propertyManager.getPropertyValue(WORKSPACE_HEADING_LABEL));

        mainPane = new BorderPane();

        topPane = new VBox();
        topPane.getChildren().add(guiHeadingLabel);
        topPane.setAlignment(Pos.CENTER);

        leftPane = new VBox();
        rightPane = new VBox();
        centerPane = new BorderPane();
        bottomPane = new VBox();

        leftPane.setPrefWidth(220);
        rightPane.setPrefWidth(220);

        mainPane.setTop(topPane);
        mainPane.setLeft(leftPane);
        mainPane.setRight(rightPane);
        mainPane.setCenter(centerPane);
        mainPane.setBottom(bottomPane);

        workspace = new VBox();
        workspace.getChildren().addAll(mainPane);
    }

    private void initExitButton() {
        PropertyManager propertyManager = PropertyManager.getManager();
        Stage primaryStage = gui.getWindow();

        primaryStage.setOnCloseRequest(new EventHandler<WindowEvent>() {
            @Override
            public void handle(WindowEvent event) {
                event.consume();
                controller.handleExitRequest();
            }
        });
    }

    public void initHomeScreen() {
        PropertyManager propertyManager = PropertyManager.getManager();

        reinitialize();

        TilePane buttonBox = new TilePane(Orientation.VERTICAL);
        buttonBox.setVgap(10);
        buttonBox.setAlignment(Pos.CENTER);

        createProfile = new Button(propertyManager.getPropertyValue(ITEM_FILE_PROFILE));
        login = new Button(propertyManager.getPropertyValue(ITEM_FILE_LOGIN));
        exit = new Button(propertyManager.getPropertyValue(ITEM_FILE_EXIT));
        createProfile.setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);
        login.setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);
        exit.setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);
        createProfile.setAlignment(Pos.CENTER);
        login.setAlignment(Pos.CENTER);
        exit.setAlignment(Pos.CENTER);

        buttonBox.getChildren().addAll(createProfile, login, exit);

        leftPane.getChildren().addAll(buttonBox);

        createProfile.setOnAction(e -> {
            try {
                controller.handleProfileRequest();
            } catch (IOException e1) {
                e1.printStackTrace();
            }
        });
        login.setOnAction(e -> {
            try {
                controller.handleLoginRequest();
            } catch (IOException e1) {
                e1.printStackTrace();
            }
        });
        exit.setOnAction(e -> controller.handleExitRequest());
    }

    /**
     * This function specifies the CSS for all the UI components known at the time the workspace is initially
     * constructed. Components added and/or removed dynamically as the application runs need to be set up separately.
     */
    @Override
    public void initStyle() {
        PropertyManager propertyManager = PropertyManager.getManager();

        gui.getAppPane().setId(propertyManager.getPropertyValue(ROOT_BORDERPANE_ID));

        workspace.getStyleClass().add(CLASS_BORDERED_PANE);
        guiHeadingLabel.getStyleClass().setAll(propertyManager.getPropertyValue(HEADING_LABEL));
    }

    /** This function reloads the entire workspace */
    @Override
    public void reloadWorkspace() {
        /* does nothing; use reinitialize() instead */
    }

    public VBox getTopPane() { return topPane; }

    public BorderPane getCenterPane() {
        return centerPane;
    }

    public VBox getRightPane() {
        return rightPane;
    }

    public VBox getLeftPane() { return leftPane; }

    public VBox getBottomPane() { return bottomPane; }

    public void reinitialize() {
        leftPane = new VBox();
        rightPane = new VBox();
        centerPane = new BorderPane();
        bottomPane = new VBox();
        leftPane.setPrefWidth(220);
        rightPane.setPrefWidth(220);
        mainPane.setLeft(leftPane);
        mainPane.setRight(rightPane);
        mainPane.setCenter(centerPane);
        mainPane.setBottom(bottomPane);
    }
}
