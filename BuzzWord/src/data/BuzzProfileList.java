package data;

import profile.*;
import java.util.*;

/**
 * Created by Anthony on 11/28/2016.
 */
public class BuzzProfileList extends ProfileList {
    private ArrayList<BuzzProfileData> profiles;

    public BuzzProfileList() {
        profiles = new ArrayList<BuzzProfileData>();
    }

    @Override
    public boolean containsUser(String user) {
        for (int i = 0; i < profiles.size(); i++) {
            if (user.equals(profiles.get(i).getUsername())) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean add(Object e) {
        BuzzProfileData pro = (BuzzProfileData) e;
        String user = pro.getUsername();
        boolean isDupe = containsUser(user);
        boolean addedToList = false;
        if (!isDupe)
            addedToList = profiles.add(pro);
        return addedToList;
    }

    @Override
    public void add(int index, Object e) {
        BuzzProfileData pro = (BuzzProfileData) e;
        String user = pro.getUsername();
        boolean isDupe = containsUser(user);
        if (!isDupe)
            profiles.add(index, pro);
    }

    public BuzzProfileData getUser(String user) {
        for (int i = 0; i < profiles.size(); i++) {
            BuzzProfileData data = profiles.get(i);
            if (user.equals(data.getUsername()))
                return data;
        }
        return null;
    }

    public BuzzProfileData bestUserHighScore(int mode) {
        sortByHighScoreMode(mode);
        return profiles.get(0);
    }

    public BuzzProfileData mostPlayedUser(int mode) {
        sortByModePlays(mode);
        return profiles.get(0);
    }

    public void sortByHighScoreMode(int mode) {
        Collections.sort(profiles, new Comparator<BuzzProfileData>() {
            @Override
            public int compare(BuzzProfileData prof1, BuzzProfileData prof2) {
                int score1 = prof1.getHighScores(mode);
                int score2 = prof2.getHighScores(mode);

                return score2-score1;
            }
        });
    }

    public void sortByModePlays(int mode) {
        Collections.sort(profiles, new Comparator<BuzzProfileData>() {
            @Override
            public int compare(BuzzProfileData prof1, BuzzProfileData prof2) {
                int plays1 = prof1.getTimesPlayed(mode);
                int plays2 = prof2.getTimesPlayed(mode);

                return plays2-plays1;
            }
        });
    }
}
