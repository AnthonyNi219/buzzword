package buzzword;

/**
 * Created by Anthony on 11/14/2016.
 */
public enum BuzzWordProperties {
    WORKSPACE_HEADING_LABEL,
    ROOT_BORDERPANE_ID,
    TOP_TOOLBAR_ID,
    SEGMENTED_BUTTON_BAR,
    FIRST_TOOLBAR_BUTTON,
    LAST_TOOLBAR_BUTTON;
}
