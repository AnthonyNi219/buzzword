package data;

import apptemplate.AppTemplate;
import components.AppDataComponent;
import controller.GameError;
import profile.ProfileData;
import propertymanager.PropertyManager;
import ui.AppMessageDialogSingleton;

import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashSet;
import java.util.Random;
import java.util.Set;
import java.util.stream.Stream;

/**
 * Created by Anthony on 11/14/2016.
 */
public class BuzzProfileData extends ProfileData implements AppDataComponent {

    private int[] highScores;
    private int[] timesPlayed;
    private int[] levels;
    public AppTemplate appTemplate;

    public BuzzProfileData() {
        super();
        this.highScores = new int[4];
        this.timesPlayed = new int[4];
        this.levels = new int[4];
        for (int i = 0; i < this.levels.length; i++)
            this.levels[i] = 1;
    }

    public BuzzProfileData(String user, String pass) {
        super(user, pass);
        this.highScores = new int[4];
        this.timesPlayed = new int[4];
        this.levels = new int[4];
        for (int i = 0; i < this.levels.length; i++)
            this.levels[i] = 1;
    }

    public void setHighScores(int mode, int score) {
        this.highScores[mode] = score;
    }

    public void setTimesPlayed(int mode, int plays) {
        this.timesPlayed[mode] = plays;
    }

    public void setLevels(int mode, int level) { this.levels[mode] = level; }

    public void setHighScores(int[] scores) {
        for (int i = 0; i < 4; i++) {
            this.highScores[i] = scores[i];
        }
    }

    public void setTimesPlayed(int[] plays) {
        for (int i = 0; i < 4; i++) {
            this.timesPlayed[i] = plays[i];
        }
    }

    public void setLevels(int[] levels) {
        for (int i = 0; i < 4; i++) {
            this.levels[i] = levels[i];
        }
    }

    public int[] getHighScores() {
        return highScores;
    }

    public int[] getTimesPlayed() {
        return timesPlayed;
    }

    public int[] getLevels() { return levels; }

    public int getHighScores(int mode) {
        return highScores[mode];
    }

    public int getTimesPlayed(int mode) {
        return timesPlayed[mode];
    }

    public int getLevels(int mode) { return levels[mode]; }

    @Override
    public void reset() {
        super.reset();
        this.highScores = new int[4];
        this.timesPlayed = new int[4];
        this.levels = new int[4];
        for (int i = 0; i < this.levels.length; i++)
            this.levels[i] = 1;
    }
}
