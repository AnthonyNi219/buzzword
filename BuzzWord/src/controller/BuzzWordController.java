package controller;

import apptemplate.AppTemplate;
import data.*;
import gui.Workspace;
import javafx.animation.AnimationTimer;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.*;
import javafx.geometry.Insets;
import javafx.geometry.Orientation;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.shape.*;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import javafx.util.Pair;
import propertymanager.PropertyManager;
import timer.Countdown;
import ui.AppMessageDialogSingleton;
import ui.YesNoCancelDialogSingleton;
import word.Word;
import word.WordList;

import java.io.*;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Optional;
import java.util.Random;

import static settings.AppPropertyType.*;
import static settings.InitializationParameters.*;

/**
 * Created by Anthony on 11/14/2016.
 */
public class BuzzWordController implements FileController {

    public enum GameState {
        UNINITIALIZED,
        LOGGED_IN_NO_GAME,
        LOGGED_IN_GAME,
        ENDED
    }

    private AppTemplate appTemplate; // shared reference to the application
    private BuzzProfileData    profiledata;    // shared reference to the game being played, loaded or saved
    private GameState   gamestate;   // the state of the game being shown in the workspace
    private Path        workFile;
    private BuzzProfileList userFiles;
    private WordList dictionary;
    private WordList answerKey;
    private ObservableList<Word> foundWords;
    private Countdown gameTimer;
    private boolean firstLetter;

    public BuzzWordController(AppTemplate appTemplate) {
        this.appTemplate = appTemplate;
        this.gamestate = GameState.UNINITIALIZED;
    }

    public void setGameState(GameState gamestate) {
        this.gamestate = gamestate;
    }

    public GameState getGamestate() {
        return this.gamestate;
    }

    private void setDialogWindowIcon(Dialog window) {
        try {
            Stage stage = (Stage) window.getDialogPane().getScene().getWindow();
            Image logoIcon = appTemplate.getGUI().getImage(APP_LOGO.toString(), 100, 100, true, true);
            stage.getIcons().add(logoIcon);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private Dialog<Pair<String, String>> createUserForm(String title, String message, String icon, String button) throws IOException {
        PropertyManager propertyManager = PropertyManager.getManager();

        //Custom Dialog
        Dialog<Pair<String, String>> dialog = new Dialog<>();
        dialog.setTitle(propertyManager.getPropertyValue(title));
        dialog.setHeaderText(propertyManager.getPropertyValue(message));

        //Icon in Middle
        Image iconImage = appTemplate.getGUI().getImage(icon, 100, 100, true, true);
        dialog.setGraphic(new ImageView(iconImage));
        //Custom Icon In Window Title
        setDialogWindowIcon(dialog);

        //Buttons
        ButtonType createButtonType = new ButtonType(propertyManager.getPropertyValue(button), ButtonBar.ButtonData.OK_DONE);
        dialog.getDialogPane().getButtonTypes().addAll(createButtonType, ButtonType.CANCEL);

        //Form
        GridPane form = new GridPane();
        form.setHgap(10);
        form.setVgap(10);
        form.setPadding(new Insets(20, 150, 10, 10));

        TextField username = new TextField();
        username.setPromptText(propertyManager.getPropertyValue(USERNAME));
        PasswordField password = new PasswordField();
        password.setPromptText(propertyManager.getPropertyValue(PASSWORD));

        Label userLabel = new Label(propertyManager.getPropertyValue(USERNAMEC));
        Label passLabel = new Label(propertyManager.getPropertyValue(PASSWORDC));

        form.add(userLabel, 0, 0);
        form.add(username, 1, 0);
        form.add(passLabel, 0, 1);
        form.add(password, 1, 1);

        // Enable/Disable login button depending on whether a username was entered.
        Node createButton = dialog.getDialogPane().lookupButton(createButtonType);
        createButton.setDisable(true);

        //Validation of username and password field if empty or not
        username.textProperty().addListener((observable, oldValue, newValue) -> {
            password.textProperty().addListener((observable1, oldValue1, newValue1) -> {
                createButton.setDisable(newValue.trim().isEmpty() && newValue1.trim().isEmpty());
            });
        });

        dialog.getDialogPane().setContent(form);

        //Focus on username field by default
        Platform.runLater(() -> username.requestFocus());

        return dialog;
    }

    public void createAlert(Alert.AlertType type, String title, String header, String content) {
        PropertyManager props = PropertyManager.getManager();
        AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();

        Alert alert = new Alert(type);
        alert.setTitle(title);
        alert.setHeaderText(header);
        alert.setContentText(content);
        //Custom Icon In Window Title
        setDialogWindowIcon(alert);
        alert.showAndWait();
    }

    public String createYesNoAlert(String title, String header, String content) {
        PropertyManager props = PropertyManager.getManager();

        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle(title);
        alert.setHeaderText(header);
        alert.setContentText(content);
        //Custom Icon In Window Title
        setDialogWindowIcon(alert);

        ButtonType yesButton = new ButtonType(props.getPropertyValue(YES));
        ButtonType noButton = new ButtonType(props.getPropertyValue(NO));
        ButtonType cancelButton = new ButtonType(props.getPropertyValue(CANCEL), ButtonBar.ButtonData.CANCEL_CLOSE);
        alert.getButtonTypes().setAll(yesButton, noButton, cancelButton);

        Optional<ButtonType> result = alert.showAndWait();
        if (result.get() == yesButton) {
            return props.getPropertyValue(YES);
        } else if (result.get() == noButton) {
            return props.getPropertyValue(NO);
        } else {
            return props.getPropertyValue(CANCEL);
        }
    }

    public void createProfileGraphic() throws IOException {
        Dialog<Pair<String,String>> dialog = createUserForm(CREATE_PROFILE_TITLE.toString(),
                CREATE_PROFILE_MESSAGE.toString(), PROFILE_CREATE.toString(), ITEM_FILE_PROFILE.toString());

        ButtonType createButtonType = dialog.getDialogPane().getButtonTypes().get(0);
        GridPane form = (GridPane) dialog.getDialogPane().getContent();
        TextField username = (TextField) getNodeFromGridPane(form, 1, 0);
        TextField password = (TextField) getNodeFromGridPane(form, 1, 1);

        //Action on create profile button press
        dialog.setResultConverter(dialogButton -> {
            if (dialogButton == createButtonType) {
                try {
                    createProfile(username.getText(), password.getText());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            return null;
        });

        dialog.showAndWait();
    }

    public void createLoginGraphic() throws IOException {
        Dialog<Pair<String, String>> dialog = createUserForm(LOGIN_PROFILE_TITLE.toString(),
                LOGIN_PROFILE_MESSAGE.toString(), LOGIN_LARGE.toString(), ITEM_FILE_LOGIN.toString());

        ButtonType createButtonType = dialog.getDialogPane().getButtonTypes().get(0);
        GridPane form = (GridPane) dialog.getDialogPane().getContent();
        TextField username = (TextField) getNodeFromGridPane(form, 1, 0);
        TextField password = (TextField) getNodeFromGridPane(form, 1, 1);

        //Action on create profile button press
        dialog.setResultConverter(dialogButton -> {
            if (dialogButton == createButtonType) {
                try {
                    login(username.getText(), password.getText());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            return null;
        });

        dialog.showAndWait();
    }

    public void createLoggedInGraphic() throws IOException {
        PropertyManager propertyManager = PropertyManager.getManager();
        Workspace workspace = (Workspace) appTemplate.getWorkspaceComponent();
        workspace.reinitialize();

        TilePane buttonBox = new TilePane(Orientation.VERTICAL);
        buttonBox.setVgap(10);
        buttonBox.setAlignment(Pos.CENTER);

        Label usernameLabel = new Label(profiledata.getUsername());
        Button logout = new Button(propertyManager.getPropertyValue(ITEM_FILE_LOGOUT));
        ObservableList<String> options = FXCollections.observableArrayList(
                propertyManager.getPropertyValue(MODE1),
                propertyManager.getPropertyValue(MODE2),
                propertyManager.getPropertyValue(MODE3),
                propertyManager.getPropertyValue(MODE4)
        );
        ComboBox modeDropDown = new ComboBox(options);
        Button play = new Button(propertyManager.getPropertyValue(ITEM_GAME_PLAY));
        Button exit = new Button(propertyManager.getPropertyValue(ITEM_FILE_EXIT));
        logout.setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);
        modeDropDown.setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);
        play.setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);
        exit.setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);
        logout.setAlignment(Pos.CENTER);
        play.setAlignment(Pos.CENTER);
        exit.setAlignment(Pos.CENTER);

        modeDropDown.setValue(propertyManager.getPropertyValue(ITEM_GAME_MODE));
        modeDropDown.setVisibleRowCount(4);

        buttonBox.getChildren().addAll(usernameLabel, logout, modeDropDown, play, exit);

        workspace.getLeftPane().getChildren().addAll(buttonBox);

        logout.setOnAction(e -> handleLogoutRequest());
        play.setOnAction(e -> {
            createLevelSelectorGraphic(modeDropDown.getValue().toString());
        });
        exit.setOnAction(e -> handleExitRequest());
    }

    public void createLevelSelectorGraphic(String modeName) {
        PropertyManager props = PropertyManager.getManager();
        Workspace workspace = (Workspace) appTemplate.getWorkspaceComponent();

        int modeNum = checkGameModeNumber(modeName);

        if (modeNum < 0) {
            createAlert(Alert.AlertType.ERROR, props.getPropertyValue(MODE_ERROR_TITLE), null, props.getPropertyValue(MODE_ERROR_MESSAGE));
            return;
        }

        workspace.reinitialize();

        //Left Screen
        VBox leftScreen = workspace.getLeftPane();

        TilePane buttonBox = new TilePane(Orientation.VERTICAL);
        buttonBox.setVgap(10);
        buttonBox.setAlignment(Pos.CENTER);

        Button logout = new Button(props.getPropertyValue(ITEM_FILE_LOGOUT));
        Button home = new Button(props.getPropertyValue(ITEM_GAME_HOME));
        Button exit = new Button(props.getPropertyValue(ITEM_FILE_EXIT));
        logout.setOnAction(e -> handleLogoutRequest());
        home.setOnAction(e -> {
            try {
                returnToHomeMenu();
            } catch (IOException e1) {
                e1.printStackTrace();
            }
        });
        exit.setOnAction(e -> handleExitRequest());

        logout.setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);
        home.setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);
        exit.setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);
        logout.setAlignment(Pos.CENTER);
        home.setAlignment(Pos.CENTER);
        exit.setAlignment(Pos.CENTER);

        buttonBox.getChildren().addAll(logout, home, exit);
        leftScreen.getChildren().addAll(buttonBox);

        //Center Screen
        BorderPane centerScreen = workspace.getCenterPane();

        HBox modeNameBox = new HBox();
        Label modeNameLabel = new Label(modeName);
        modeNameLabel.setId("mode-name-label");
        modeNameBox.getChildren().addAll(modeNameLabel);
        modeNameBox.setAlignment(Pos.CENTER);

        GridPane levelCircles = new GridPane();
        levelCircles.setAlignment(Pos.CENTER);
        levelCircles.setHgap(5);
        levelCircles.setVgap(5);

        int levelsUnlocked = profiledata.getLevels(modeNum);
        int numOfLevels = 1;
        for (int i = 0; i < 2; i++) {
            for (int j = 0; j < 4; j++) {
                Text levelNum = new Text(Integer.toString(numOfLevels));
                StackPane levelButton;
                if (levelsUnlocked > 0) {
                    levelButton = makeLetterCircles(levelNum, 25, Color.BLACK, 30, Color.LIGHTBLUE);
                    levelButton.setOnMouseClicked(e -> createBuzzGameGraphic(modeName, levelNum.getText()));
                    levelButton.setDisable(false);
                }
                else {
                    levelButton = makeLetterCircles(levelNum, 25, Color.WHITE, 30, Color.DARKBLUE);
                }
                levelCircles.add(levelButton, j, i, 1, 1);
                numOfLevels++;
                levelsUnlocked--;
            }
        }

        centerScreen.setTop(modeNameBox);
        centerScreen.setCenter(levelCircles);
    }

    public void createBuzzGameGraphic(String modeName, String level) {
        PropertyManager props = PropertyManager.getManager();
        Workspace workspace = (Workspace) appTemplate.getWorkspaceComponent();

        int modeNum = checkGameModeNumber(modeName);
        try {
            loadDictionary(modeName);
        } catch (IOException e) {
            e.printStackTrace();
        }

        BorderPane centerScreen = workspace.getCenterPane();
        VBox bottomScreen = workspace.getBottomPane();
        VBox rightScreen = workspace.getRightPane();

        //Clear workspace
        centerScreen.getChildren().clear();
        bottomScreen.getChildren().clear();
        rightScreen.getChildren().clear();

        //Center Screen
        //Center Top
        HBox modeNameBox = new HBox();
        Label modeNameLabel = new Label(modeName);
        modeNameBox.getChildren().addAll(modeNameLabel);
        modeNameBox.setAlignment(Pos.CENTER);

        //Center Center
        GridPane gameCircles = new GridPane();
        gameCircles.setAlignment(Pos.CENTER);
        createGameBoard(gameCircles, level);

        //Center Bottom
        VBox labelButtonBox = new VBox();
        labelButtonBox.setAlignment(Pos.CENTER);
        labelButtonBox.setSpacing(10);

        HBox levelLabelBox = new HBox();
        levelLabelBox.setSpacing(4);
        levelLabelBox.setAlignment(Pos.CENTER);
        Label levelWordLabel = new Label(props.getPropertyValue(LEVEL_LABEL));
        Label levelNumLabel = new Label(level);
        levelLabelBox.getChildren().addAll(levelWordLabel, levelNumLabel);

        labelButtonBox.getChildren().addAll(levelLabelBox);

        //Bottom Screen
        TilePane playPauseBox = new TilePane();
        playPauseBox.setAlignment(Pos.CENTER);
        Button play = new Button(props.getPropertyValue(ITEM_GAME_PLAY));
        play.setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);
        play.setAlignment(Pos.CENTER);
        playPauseBox.getChildren().addAll(play);
        play.setOnAction(e -> playGame());

        bottomScreen.getChildren().addAll(playPauseBox);
        centerScreen.setTop(modeNameBox);
        centerScreen.setCenter(gameCircles);
        centerScreen.setBottom(labelButtonBox);

        //Right Screen

        //Timer
        createGameTimer(60);

        HBox timerBox = new HBox();
        timerBox.setAlignment(Pos.CENTER);
        Label remaining = new Label(props.getPropertyValue(TIMER_LABEL_REMAIN));
        Label timeCount = new Label();
        timeCount.textProperty().bind(gameTimer.getTimeSeconds().asString());
        Label measurement = new Label(props.getPropertyValue(TIMER_LABEL_TIME));
        timerBox.getChildren().addAll(remaining, timeCount, measurement);
        timerBox.setSpacing(4);

        StackPane selectedLetters = new StackPane();
        selectedLetters.setPrefSize(200, 40);
        selectedLetters.setMaxSize(200,40);
        selectedLetters.setAlignment(Pos.CENTER);
        Rectangle boxBackground = new Rectangle(selectedLetters.getPrefWidth(), selectedLetters.getPrefHeight(), Color.SILVER);
        //Add letters into letterBox
        HBox letterBox = new HBox();
        letterBox.setAlignment(Pos.CENTER);
        letterBox.setSpacing(2);
        selectedLetters.getChildren().addAll(boxBackground, letterBox);

        //Add words to foundWords observableArrayList
        foundWords = FXCollections.observableArrayList();

        VBox scoreboard = new VBox();
        TableView guessedWordsTable = new TableView();
        guessedWordsTable.setMaxSize(200, 250);
        guessedWordsTable.setPrefSize(200, 250);
        guessedWordsTable.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);
        guessedWordsTable.setEditable(false);
        TableColumn wordsCol = new TableColumn(props.getPropertyValue(TABLE_WORDS));
        TableColumn scoresCol = new TableColumn(props.getPropertyValue(TABLE_SCORES));
        wordsCol.prefWidthProperty().bind(guessedWordsTable.widthProperty().multiply(0.7));
        scoresCol.prefWidthProperty().bind(guessedWordsTable.widthProperty().multiply(0.3));
        wordsCol.setResizable(false);
        scoresCol.setResizable(false);
        wordsCol.setCellValueFactory(new PropertyValueFactory<>("word"));
        scoresCol.setCellValueFactory(new PropertyValueFactory<>("score"));
        guessedWordsTable.setItems(foundWords);
        guessedWordsTable.getColumns().addAll(wordsCol, scoresCol);
        HBox totalBox = new HBox();
        totalBox.setMaxSize(200,30);
        totalBox.setPrefSize(200, 30);
        Label totalString = new Label(props.getPropertyValue(STRING_TOTAL));
        //Need to manually update this each time word found
        Label foundPoints = new Label(Integer.toString(getTotalScore()));
        totalString.prefWidthProperty().bind(totalBox.widthProperty().multiply(0.7));
        foundPoints.prefWidthProperty().bind(totalBox.widthProperty().multiply(0.3));
        totalBox.getChildren().addAll(totalString, foundPoints);
        scoreboard.getChildren().addAll(guessedWordsTable, totalBox);


        VBox targetPane = new VBox();
        Label targetString = new Label(props.getPropertyValue(STRING_TARGET));
        targetString.setId("target-string");
        HBox pointsLine = new HBox();
        pointsLine.setSpacing(4);
        pointsLine.setId("target-points");
        Label numPoints = new Label(answerKey.getTotalScoreInList() + "");
        Label pointsString = new Label(props.getPropertyValue(TABLE_SCORES));
        pointsLine.getChildren().addAll(numPoints, pointsString);
        targetPane.getChildren().addAll(targetString, pointsLine);

        rightScreen.getChildren().addAll(timerBox, selectedLetters, scoreboard, targetPane);
        rightScreen.setSpacing(20);
    }

    private Node getNodeFromGridPane(GridPane gridPane, int col, int row) {
        for (Node node : gridPane.getChildren()) {
            if (GridPane.getColumnIndex(node) == col && GridPane.getRowIndex(node) == row) {
                return node;
            }
        }
        return null;
    }

    private void createGameBoard(GridPane board, String level) {
        int difficulty = Integer.parseInt(level);
        int numRowsCols = 4;
        boolean firstWord = true;
        answerKey = new WordList();

        if (difficulty > 0 && difficulty <= 4) {
            for (int i = 0; i < (difficulty / 2 + 1); i++) {
                Word picked = getRandomWord((int) (Math.random() * 2 + 3));
                answerKey.add(picked);
            }
            numRowsCols = 4;
        } else if (difficulty > 4 && difficulty <= 6) {
            for (int i = 0; i < (difficulty / 2 + 1); i++) {
                Word picked = getRandomWord((int) (Math.random() * 4 + 3));
                answerKey.add(picked);
            }
            numRowsCols = 5;
        } else if (difficulty > 6 && difficulty <= 8) {
            for (int i = 0; i < (difficulty / 2 + 1); i++) {
                Word picked = getRandomWord((int) (Math.random() * 13 + 3));
                answerKey.add(picked);
            }
            numRowsCols = 6;
        }

        for (int keySize = 0; keySize < answerKey.size() ; keySize++) {
            Word temp = (Word) answerKey.get(keySize);
            String tempWordName = temp.getWord();
            System.out.println(tempWordName);
            if (firstWord) {
                int startingCol = (int) ((Math.random() * numRowsCols * 2) / 2);
                int startingRow = (int) ((Math.random() * numRowsCols * 2) / 2);
                startingCol = startingCol * 2;
                startingRow = startingRow * 2;
                insertWordIntoBoard(board, tempWordName, startingCol, startingRow, numRowsCols, new Random().nextBoolean());
                firstWord = false;
            } else {
                int[] coords = findEmptySpace(board, numRowsCols, tempWordName.length());
                int row = coords[0];
                int col = coords[1];
                if (col >= 0 && row >= 0) {
                    boolean inserted = insertWordIntoBoard(board, tempWordName, col, row, numRowsCols, false);
                    if (!inserted)
                        answerKey.remove(keySize);
                } else {
                    answerKey.remove(keySize);
                }
            }
        }

        int rowsCols = (numRowsCols * 2) - 2;

        for (int i = 0; i < (numRowsCols * 2); i+=2) {
            for (int j = 0; j < (numRowsCols * 2); j+=2) {
                Node letterNode = getNodeFromGridPane(board, j, i);
                if (letterNode == null) {
                    StackPane circleLetter = makeLetterCircles(randomLetter(), 25, Color.BLACK, 25, Color.LIGHTBLUE);
                    circleLetter.setOnMouseClicked(e -> selectLetter(circleLetter, rowsCols));
                    circleLetter.setOnMouseDragged(e -> selectLetter(circleLetter, rowsCols));
                    board.add(circleLetter, j , i, 1, 1);
                }
            }
        }

        //Create Horizontal Edges
        for (int i = 0; i < numRowsCols * 2; i+=2) {
            for (int j = 1; j < (numRowsCols * 2) - 2; j+=2) {
                StackPane lineBox = new StackPane();
                lineBox.setPrefSize(5,50);
                lineBox.setMaxSize(5,50);
                lineBox.setMinSize(5,50);
                lineBox.setAlignment(Pos.CENTER);
                Line edge = new Line(0,2.5,5,2.5);
                lineBox.getChildren().addAll(edge);
                board.add(lineBox, j, i, 1, 1);
            }
        }

        //Create Vertical Edges
        for (int i = 0; i < numRowsCols * 2; i+=2) {
            for (int j = 1; j < (numRowsCols * 2) - 2; j+=2) {
                StackPane lineBox = new StackPane();
                lineBox.setPrefSize(50,5);
                lineBox.setMaxSize(50,5);
                lineBox.setMinSize(50,5);
                lineBox.setAlignment(Pos.CENTER);
                Line edge = new Line(25, 0, 25, 5);
                lineBox.getChildren().addAll(edge);
                board.add(lineBox, i, j, 1,1);
            }
        }
    }

    private boolean insertWordIntoBoard(GridPane board, String word, int col, int row, int numRowsCols, boolean orientation) {
        //orientation; true = row, false = col
        int rowIndex = row;
        int colIndex = col;
        int wordIndex = 0;
        boolean reversed = false;
        int spaceRemaining = 0;
        boolean wrapDown;
        numRowsCols = (numRowsCols * 2) - 2;
        int rowsCols = numRowsCols;

        if (!orientation) {
            rowIndex = row;
            colIndex = col;
        }
        else {
            rowIndex = col;
            colIndex = row;
        }
        while (!((rowIndex > numRowsCols) && (colIndex > numRowsCols))) {
            spaceRemaining++;
            if (!reversed) {
                if (rowIndex <= numRowsCols) {
                    if (colIndex < numRowsCols) {
                        colIndex += 2;
                    } else if (rowIndex < numRowsCols) {
                        rowIndex += 2;
                        reversed = true;
                    } else
                        break;
                }
            } else {
                if (rowIndex <= numRowsCols) {
                    if (colIndex > 0) {
                        colIndex -= 2;
                    } else if (rowIndex < numRowsCols) {
                        rowIndex += 2;
                        reversed = false;
                    } else
                        break;
                }
            }
        }

        if (spaceRemaining >= word.length())
            wrapDown = true;
        else
            wrapDown = false;

        //Reset variables
        reversed = false;
        spaceRemaining = 0;
        if (!orientation) {
            rowIndex = row;
            colIndex = col;
        }
        else {
            rowIndex = col;
            colIndex = row;
        }

        while (wordIndex < word.length()) {
            Text letterOfWord = new Text(Character.toString(word.charAt(wordIndex)));
            StackPane circleLetter = makeLetterCircles(letterOfWord, 25, Color.BLACK, 25, Color.LIGHTBLUE);
            circleLetter.setOnMouseClicked(e -> selectLetter(circleLetter, rowsCols));
            circleLetter.setOnMouseDragged(e -> selectLetter(circleLetter, rowsCols));
            Node emptyNode = getNodeFromGridPane(board, colIndex, rowIndex);
            if (emptyNode == null) {
                board.add(circleLetter, colIndex, rowIndex, 1, 1);
                wordIndex++;
            } else {
                return false;
            }
            if (wrapDown) {
                if (!reversed) {
                    if (rowIndex <= numRowsCols) {
                        if (colIndex < numRowsCols) {
                            colIndex += 2;
                        } else if (rowIndex < numRowsCols) {
                            rowIndex += 2;
                            reversed = true;
                        }
                    }
                } else {
                    if (rowIndex <= numRowsCols) {
                        if (colIndex > 0) {
                            colIndex -= 2;
                        } else if (rowIndex < numRowsCols) {
                            rowIndex += 2;
                            reversed = false;
                        }
                    }
                }
            } else {
                if (!reversed) {
                    if (rowIndex >= 0) {
                        if (colIndex > 0) {
                            colIndex -= 2;
                        } else if (rowIndex > 0) {
                            rowIndex -= 2;
                            reversed = true;
                        }
                    }
                } else {
                    if (rowIndex >= 0) {
                        if (colIndex < numRowsCols) {
                            colIndex += 2;
                        } else if (rowIndex > 0) {
                            rowIndex -= 2;
                            reversed = false;
                        }
                    }
                }
            }
        }
        return true;
    }

    private int[] findEmptySpace(GridPane board, int numRowsCols, int wordlength) {
        int[] coords = new int[]{-1,-1};
        numRowsCols = (numRowsCols * 2) - 2;
        int startRow = 0;
        int startCol = 0;
        int endRow = numRowsCols;
        int endCol = numRowsCols;
        int rowIndex = startRow;
        int colIndex = startCol;
        boolean reversed = false;
        boolean firstNode = true;
        int emptySpace = 0;

        while (!((startRow > numRowsCols) && (startCol > numRowsCols))) {
            if (emptySpace >= wordlength)
                return coords;
            Node curNode = getNodeFromGridPane(board, startCol, startRow);
            if (curNode == null) {
                emptySpace++;
                if (firstNode) {
                    coords[0] = startRow;
                    coords[1] = startCol;
                    firstNode = false;
                }
            } else {
                emptySpace = 0;
                coords[0] = -1;
                coords[1] = -1;
                firstNode = true;
            }
            if (!reversed) {
                if (startRow <= numRowsCols) {
                    if (startCol < numRowsCols) {
                        startCol += 2;
                    } else if (startRow < numRowsCols) {
                        startRow += 2;
                        reversed = true;
                    } else
                        break;
                }
            } else {
                if (startRow <= numRowsCols) {
                    if (startCol > 0) {
                        startCol -= 2;
                    } else if (startRow < numRowsCols) {
                        startRow += 2;
                        reversed = false;
                    } else
                        break;
                }
            }
        }



        return coords;
    }

    private Word getRandomWord(int wordlength) {
        Word picked;
        int randNum = (int) (Math.random() * dictionary.size());
        picked = (Word) dictionary.get(randNum);
        while (picked.getWord().length() != wordlength) {
            picked = (Word) dictionary.get((int) (Math.random() * dictionary.size()));
        }
        return picked;
    }

    private StackPane makeLetterCircles(Text letter, double textSize, Color textColor, double circleRadius, Color boxColor) {
        letter.setFont(Font.font("System", textSize));
        letter.setFill(textColor);
        letter.setStyle("-fx-font-weight: bold;");

        StackPane holdingLetter = new StackPane();

        Circle circ = new Circle();
        circ.setRadius(circleRadius);
        circ.setFill(boxColor);

        holdingLetter.getChildren().addAll(circ, letter);
        holdingLetter.setAlignment(Pos.CENTER);

        return holdingLetter;
    }

    private Text randomLetter() {
        Random r = new Random();
        char c = (char)(r.nextInt(26) + 'A');
        String s = "" + c;
        Text letter = new Text(s);
        return letter;
    }

    private int getTotalScore() {
        int score = 0;

        for (int i = 0; i < foundWords.size(); i++)
            score+= foundWords.get(i).getScore();

        return score;
    }

    private void createGameTimer(Integer time) {
        EventHandler onFinished = new EventHandler<ActionEvent>() {
            public void handle(ActionEvent t) {
                gameTimer.stopTimer();
                end();
            }
        };

        if (gameTimer != null)
            gameTimer.stopTimer();
        gameTimer = new Countdown(time, onFinished);
    }

    public void pauseGame() {
        PropertyManager props = PropertyManager.getManager();
        Workspace workspace = (Workspace) appTemplate.getWorkspaceComponent();
        BorderPane centerScreen = workspace.getCenterPane();
        VBox bottomScreen = workspace.getBottomPane();
        GridPane gameGrid = (GridPane) centerScreen.getCenter();
        TilePane playPauseBox = (TilePane) bottomScreen.getChildren().get(0);

        //Replace Pause Button with Play Button
        playPauseBox.getChildren().clear();
        Button play = new Button(props.getPropertyValue(ITEM_GAME_PLAY));
        play.setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);
        play.setAlignment(Pos.CENTER);
        play.setVisible(true);
        play.setOnAction(e -> playGame());
        playPauseBox.getChildren().addAll(play);

        //Gameplay
        setGameState(GameState.LOGGED_IN_GAME);
        gameGrid.setVisible(false);

        //Pause timer
        gameTimer.pauseTimer();
    }

    public void playGame() {
        PropertyManager props = PropertyManager.getManager();
        Workspace workspace = (Workspace) appTemplate.getWorkspaceComponent();
        BorderPane centerScreen = workspace.getCenterPane();
        VBox bottomScreen = workspace.getBottomPane();
        GridPane gameGrid = (GridPane) centerScreen.getCenter();
        TilePane playPauseBox = (TilePane) bottomScreen.getChildren().get(0);

        //Replace Play Button with Pause Button
        playPauseBox.getChildren().clear();
        Button pause = new Button(props.getPropertyValue(ITEM_GAME_PAUSE));
        pause.setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);
        pause.setAlignment(Pos.CENTER);
        pause.setVisible(true);
        pause.setOnAction(e -> pauseGame());
        playPauseBox.getChildren().addAll(pause);

        //Gameplay
        setGameState(GameState.LOGGED_IN_GAME);
        gameGrid.setVisible(true);

        //Start timer
        if (!gameTimer.isStarted()) {
            gameTimer.startTimer();
            firstLetter = true;
            play();
        }
        else
            gameTimer.resumeTimer();
    }

    private void selectLetter(StackPane letterPane, int numRowsCols) {
        PropertyManager props = PropertyManager.getManager();
        Workspace workspace = (Workspace) appTemplate.getWorkspaceComponent();
        VBox rightScreen = workspace.getRightPane();
        StackPane selectedLetter = (StackPane) rightScreen.getChildren().get(1);
        HBox letterBox = (HBox) selectedLetter.getChildren().get(1);

        Circle circ = (Circle) letterPane.getChildren().get(0);
        Text letterText = (Text) letterPane.getChildren().get(1);
        int[] coords;
        coords = findNodeInGrid(letterPane);

        if (getGamestate().equals(GameState.LOGGED_IN_GAME)) {
            if (firstLetter) {
                if (circ.getStroke() == null) {
                    circ.setStroke(Color.YELLOW);
                    Text textToAdd = new Text(letterText.getText());
                    letterBox.getChildren().addAll(textToAdd);
                    firstLetter = false;
                }
            } else {
                boolean isAdjacent = checkAdjacentSelected(letterPane, coords, numRowsCols);
                if (isAdjacent) {
                    if (circ.getStroke() == null) {
                        circ.setStroke(Color.YELLOW);
                        Text textToAdd = new Text(letterText.getText());
                        letterBox.getChildren().addAll(textToAdd);
                    }
                }
            }
        }
        checkSelectedLetter(numRowsCols);
    }

    private void clearLetterBox(int rowsCols) {
        Workspace workspace = (Workspace) appTemplate.getWorkspaceComponent();
        VBox rightScreen = workspace.getRightPane();
        BorderPane centerScreen = workspace.getCenterPane();
        GridPane gameGrid = (GridPane) centerScreen.getCenter();
        StackPane selectedLetter = (StackPane) rightScreen.getChildren().get(1);
        HBox letterBox = (HBox) selectedLetter.getChildren().get(1);
        VBox scoreboard = (VBox) rightScreen.getChildren().get(2);

        letterBox.getChildren().clear();

        for (int i = 0; i <= rowsCols; i+=2) {
            for (int j = 0; j <= rowsCols; j+=2) {
                StackPane circlePane = (StackPane) getNodeFromGridPane(gameGrid, i, j);
                Circle circ = (Circle) circlePane.getChildren().get(0);
                circ.setStroke(null);
            }
        }



        firstLetter = true;
    }

    private void checkSelectedLetter(int rowsCols) {
        Workspace workspace = (Workspace) appTemplate.getWorkspaceComponent();
        VBox rightScreen = workspace.getRightPane();
        StackPane selectedLetter = (StackPane) rightScreen.getChildren().get(1);
        HBox letterBox = (HBox) selectedLetter.getChildren().get(1);

        String word = "";
        Word answer;
        for (Node node : letterBox.getChildren()) {
            Text letter = (Text) node;
            word += letter.getText();
        }
        if (answerKey.containsWord(word)) {
            answer = answerKey.getWord(word);
            if (!foundWords.contains(answer)) {
                foundWords.addAll(answer);
                clearLetterBox(rowsCols);
            }
        }
    }

    private boolean checkAdjacentSelected(StackPane letterPane, int[] coords, int numRowsCols) {
        Workspace workspace = (Workspace) appTemplate.getWorkspaceComponent();
        BorderPane centerScreen = workspace.getCenterPane();
        GridPane gameGrid = (GridPane) centerScreen.getCenter();
        boolean isAdjacent = false;
        int col = coords[0];
        int row = coords[1];
        StackPane circlePane;
        //Check left
        int colL = col - 2;
        if (colL >= 0) {
            circlePane = (StackPane) getNodeFromGridPane(gameGrid, colL, row);
            Circle actualCircle = (Circle) circlePane.getChildren().get(0);
            if (actualCircle.getStroke() != null)
                isAdjacent = true;
        }
        //Check right
        int colR = col + 2;
        if (colR <= numRowsCols) {
            circlePane = (StackPane) getNodeFromGridPane(gameGrid, colR, row);
            Circle circle = (Circle) circlePane.getChildren().get(0);
            if (circle.getStroke() != null)
                isAdjacent = true;
        }
        //Check top
        int rowT = row - 2;
        if (rowT >= 0) {
            circlePane = (StackPane) getNodeFromGridPane(gameGrid, col, rowT);
            Circle circ = (Circle) circlePane.getChildren().get(0);
            if (circ.getStroke() != null)
                isAdjacent = true;
        }
        //Check bottom
        int rowB = row + 2;
        if (rowB <= numRowsCols) {
            circlePane = (StackPane) getNodeFromGridPane(gameGrid, col, rowB);
            Circle circle = (Circle) circlePane.getChildren().get(0);
            if (circle.getStroke() != null)
                isAdjacent = true;

        }
        return isAdjacent;
    }

    private int[] findNodeInGrid(StackPane letterPane) {
        Workspace workspace = (Workspace) appTemplate.getWorkspaceComponent();
        BorderPane centerScreen = workspace.getCenterPane();
        GridPane gameGrid = (GridPane) centerScreen.getCenter();
        int[] coords = new int[]{-1,-1};
        for (Node node : gameGrid.getChildren()) {
            StackPane gridObject = (StackPane) node;
            if (gridObject.equals(letterPane)) {
                coords[0] = gameGrid.getColumnIndex(node);
                coords[1] = gameGrid.getRowIndex(node);
                return coords;
            }
        }
        return coords;
    }

    public void play() {
        AnimationTimer timer = new AnimationTimer() {
            @Override
            public void handle(long now) {

            }

            @Override
            public void stop() {
                super.stop();
                end();
            }
        };
    }

    private void end() {
        PropertyManager props = PropertyManager.getManager();
        Workspace workspace = (Workspace) appTemplate.getWorkspaceComponent();
        BorderPane centerScreen = workspace.getCenterPane();
        VBox bottomScreen = workspace.getBottomPane();
        GridPane gameGrid = (GridPane) centerScreen.getCenter();
        TilePane playPauseBox = (TilePane) bottomScreen.getChildren().get(0);
        HBox modeLabel = (HBox) centerScreen.getTop();
        Label mode = (Label) modeLabel.getChildren().get(0);
        String modeString = mode.getText();
        VBox levelBoxBox = (VBox) centerScreen.getBottom();
        HBox levelBox = (HBox) levelBoxBox.getChildren().get(0);
        Label level = (Label) levelBox.getChildren().get(1);
        String levelString = level.getText();

        setGameState(GameState.LOGGED_IN_NO_GAME);
        gameGrid.setVisible(false);
        gameTimer.setGameover(true);

        playPauseBox.getChildren().clear();
        Button newGame = new Button(props.getPropertyValue(NEW_GAME_BUTTON));
        newGame.setOnAction(e -> createBuzzGameGraphic(modeString, levelString));
        playPauseBox.getChildren().addAll(newGame);

        Platform.runLater(() -> createAlert(Alert.AlertType.INFORMATION, props.getPropertyValue(TIME_UP_TITLE), null, props.getPropertyValue(TIME_UP_MESSAGE)));
    }

    public int checkGameModeNumber(String modeName) {
        PropertyManager props = PropertyManager.getManager();

        if (modeName.equals(props.getPropertyValue(MODE1)))
            return 0;
        else if (modeName.equals(props.getPropertyValue(MODE2)))
            return 1;
        else if (modeName.equals(props.getPropertyValue(MODE3)))
            return 2;
        else if (modeName.equals(props.getPropertyValue(MODE4)))
            return 3;
        else
            return -1;
    }

    private void returnToHomeMenu() throws IOException {
        PropertyManager props = PropertyManager.getManager();

        if (getGamestate().equals(GameState.LOGGED_IN_GAME))
            pauseGame();

        String yesNoChoice = createYesNoAlert(props.getPropertyValue(HOME_REQ_TITLE), null, props.getPropertyValue(HOME_REQ_MESSAGE));

        if (yesNoChoice.equals(props.getPropertyValue(YES))) {
            gameTimer.stopTimer();
            createLoggedInGraphic();
        } else
            playGame();
    }

    @Override
    public void handleProfileRequest() throws IOException {
        createProfileGraphic();
    }

    @Override
    public void handleLogoutRequest() {
        PropertyManager propertyManager = PropertyManager.getManager();

        try {
            boolean logout = true;
            if (gamestate.equals(GameState.LOGGED_IN_GAME))
                logout = promptToLogout();
            if (logout)
                logout();
        } catch (IOException e) {
            createAlert(Alert.AlertType.ERROR, propertyManager.getPropertyValue(LOGOUT_ERROR_TITLE), null, propertyManager.getPropertyValue(LOGOUT_ERROR_MESSAGE));
        }
    }

    @Override
    public void handleNewRequest() {
        AppMessageDialogSingleton messageDialog   = AppMessageDialogSingleton.getSingleton();
        PropertyManager           propertyManager = PropertyManager.getManager();
        boolean                   makenew         = true;
        if (gamestate.equals(GameState.LOGGED_IN_GAME))
            try {
                makenew = promptToSave();
            } catch (IOException e) {
                messageDialog.show(propertyManager.getPropertyValue(NEW_ERROR_TITLE), propertyManager.getPropertyValue(NEW_ERROR_MESSAGE));
            }
        if (makenew) {
            appTemplate.getDataComponent().reset();                // reset the data (should be reflected in GUI)
            appTemplate.getWorkspaceComponent().reloadWorkspace(); // load data into workspace
            ensureActivatedWorkspace();                            // ensure workspace is activated
            workFile = null;                                       // new workspace has never been saved to a file
            ((Workspace) appTemplate.getWorkspaceComponent()).reinitialize();
        }
        if (gamestate.equals(GameState.ENDED)) {
            Workspace gameWorkspace = (Workspace) appTemplate.getWorkspaceComponent();
            gameWorkspace.reinitialize();
        }

    }

    @Override
    public void handleSaveRequest() throws IOException {
        PropertyManager propertyManager = PropertyManager.getManager();
        if (workFile == null) {
            if (createSaveFile() != null) {
                save(createSaveFile().toPath(), profiledata);
            }
        } else
            save(workFile, profiledata);
    }

    @Override
    public void handleLoginRequest() throws IOException {
        createLoginGraphic();
    }

    @Override
    public void handleExitRequest() {
        PropertyManager           props  = PropertyManager.getManager();

        if (gamestate.equals(GameState.LOGGED_IN_GAME))
            pauseGame();
        String result = createYesNoAlert(props.getPropertyValue(QUIT_GAME_TITLE), null, props.getPropertyValue(QUIT_GAME_MESSAGE));
        if (result.equals(props.getPropertyValue(YES)))
            Platform.exit();
        else if (gamestate.equals(GameState.LOGGED_IN_GAME))
            playGame();
    }

    private void ensureActivatedWorkspace() {
        appTemplate.getWorkspaceComponent().activateWorkspace(appTemplate.getGUI().getAppPane());
    }

    private void makeSaveFolder(Path link) {
        String path = link.toString();
        File dirFolder = new File(path);
        if (!dirFolder.exists()) {
            dirFolder.mkdirs();
        }
    }

    private File createSaveFile() {
        PropertyManager propertyManager = PropertyManager.getManager();

        Path        appDirPath  = Paths.get(propertyManager.getPropertyValue(APP_TITLE)).toAbsolutePath();
        Path        targetPath  = appDirPath.resolve(APP_WORKDIR_PATH.getParameter());
        makeSaveFolder(targetPath);
        String extension   = propertyManager.getPropertyValue(WORK_FILE_EXT);
        int profileNum = 0;
        File selectedFile = new File(targetPath.toString() + "\\Profile" + profileNum + "." + extension);
        //Check if file already exists
        while (selectedFile.exists()) {
            profileNum++;
            selectedFile = new File(targetPath.toString() + "\\Profile" + profileNum + "." + extension);
        }
        return selectedFile;
    }

    private void loadProfiles() throws IOException {
        PropertyManager propertyManager = PropertyManager.getManager();
        userFiles = new BuzzProfileList();

        Path        appDirPath  = Paths.get(propertyManager.getPropertyValue(APP_TITLE)).toAbsolutePath();
        Path        targetPath  = appDirPath.resolve(APP_WORKDIR_PATH.getParameter());
        makeSaveFolder(targetPath);
        String extension   = propertyManager.getPropertyValue(WORK_FILE_EXT);
        int profileNum = 0;
        File selectedFile = new File(targetPath.toString() + "\\Profile" + profileNum + "." + extension);
        //Check if file already exists
        while (selectedFile.exists()) {
            load(selectedFile.toPath());
            profileNum++;
            selectedFile = new File(targetPath.toString() + "\\Profile" + profileNum + "." + extension);
        }
    }

    private void loadDictionary(String modeName) throws IOException {
        PropertyManager propertyManager = PropertyManager.getManager();
        dictionary = new WordList();

        Path        appDirPath  = Paths.get(propertyManager.getPropertyValue(APP_TITLE)).toAbsolutePath();
        Path        targetPath = appDirPath.resolve(APP_WORDSDIR_PATH.getParameter());
        File selectedDictionary = new File(targetPath.toString() + "\\" + modeName + ".txt");
        BufferedReader reader = new BufferedReader(new FileReader(selectedDictionary));
        String line;
        while ((line = reader.readLine()) != null) {
            line = line.replaceAll("[^A-Za-z]","").trim().toUpperCase();
            if (line.length() <= 16 && line.length() > 0) {
                Word tempName = new Word(line);
                dictionary.add(tempName);
            }
        }
        reader.close();
    }

    private boolean promptToSave() throws IOException {
        PropertyManager            propertyManager   = PropertyManager.getManager();
        YesNoCancelDialogSingleton yesNoCancelDialog = YesNoCancelDialogSingleton.getSingleton();

        yesNoCancelDialog.show(propertyManager.getPropertyValue(SAVE_UNSAVED_WORK_TITLE),
                propertyManager.getPropertyValue(SAVE_UNSAVED_WORK_MESSAGE));

        if (yesNoCancelDialog.getSelection().equals(YesNoCancelDialogSingleton.YES))
            handleSaveRequest();

        return !yesNoCancelDialog.getSelection().equals(YesNoCancelDialogSingleton.CANCEL);
    }

    private boolean promptToLogout() throws IOException {
        PropertyManager            propertyManager   = PropertyManager.getManager();

        if (getGamestate().equals(GameState.LOGGED_IN_GAME))
            pauseGame();

        String yesNoChoice = createYesNoAlert(propertyManager.getPropertyValue(LOGOUT_TITLE), null, propertyManager.getPropertyValue(LOGOUT_MESSAGE));

        if (yesNoChoice.equals(propertyManager.getPropertyValue(YES))) {
            gameTimer.stopTimer();
            return true;
        }

        playGame();
        return false;
    }

    /**
     * A helper method to save work. It saves the work, marks the current work file as saved, notifies the user, and
     * updates the appropriate controls in the user interface
     *
     * @param target The file to which the work will be saved.
     * @throws IOException
     */
    private void save(Path target, BuzzProfileData data) throws IOException {
        appTemplate.getFileComponent().saveData(data, target);
        workFile = target;
        setGameState(GameState.LOGGED_IN_NO_GAME);
        AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
        PropertyManager           props  = PropertyManager.getManager();
        dialog.show(props.getPropertyValue(SAVE_COMPLETED_TITLE), props.getPropertyValue(SAVE_COMPLETED_MESSAGE));
    }

    private void create(Path target, BuzzProfileData data) throws IOException {
        appTemplate.getFileComponent().saveData(data, target);
        PropertyManager           props  = PropertyManager.getManager();
        createAlert(Alert.AlertType.INFORMATION, props.getPropertyValue(CREATED_TITLE), null, props.getPropertyValue(CREATED_MESSAGE));
    }

    /**
     * A helper method to load saved game data. It loads the game data, notified the user, and then updates the GUI to
     * reflect the correct state of the game.
     *
     * @param source The source data file from which the game is loaded.
     * @throws IOException
     */
    private void load(Path source) throws IOException {
        BuzzProfileData data = (BuzzProfileData) appTemplate.getFileComponent().loadData(source);
        if (data != null)
            userFiles.add(data);
    }

    private void createProfile(String user, String pass) throws IOException {
        PropertyManager propertyManager = PropertyManager.getManager();

        loadProfiles();
        boolean dupe = userFiles.containsUser(user);

        if (!dupe) {
            BuzzProfileData profile = new BuzzProfileData(user, pass);
            if (createSaveFile() != null) {
                create(createSaveFile().toPath(), profile);
            }
        } else {
            createAlert(Alert.AlertType.WARNING, propertyManager.getPropertyValue(PROFILE_EXISTS_TITLE), null, propertyManager.getPropertyValue(PROFILE_EXISTS_MESSAGE));
            handleProfileRequest();
        }
    }

    private void login(String user, String pass) throws IOException {
        PropertyManager propertyManager = PropertyManager.getManager();

        loadProfiles();
        BuzzProfileData userData = userFiles.getUser(user);

        if (userData != null) {
            boolean isUser = userData.checkPassword(pass);
            if (isUser) {
                profiledata = userData;
                setGameState(GameState.LOGGED_IN_NO_GAME);
                createLoggedInGraphic();
                createAlert(Alert.AlertType.INFORMATION, propertyManager.getPropertyValue(LOGGED_IN_TITLE), null, propertyManager.getPropertyValue(LOGGED_IN_MESSAGE));
            } else {
                createAlert(Alert.AlertType.WARNING, propertyManager.getPropertyValue(INVALID_LOGIN_TITLE), null, propertyManager.getPropertyValue(INVALID_LOGIN_MESSAGE));
                handleLoginRequest();
            }
        } else {
            createAlert(Alert.AlertType.WARNING, propertyManager.getPropertyValue(INVALID_LOGIN_TITLE), null, propertyManager.getPropertyValue(INVALID_LOGIN_MESSAGE));
            handleLoginRequest();
        }
    }

    private void logout() {
        PropertyManager propertyManager = PropertyManager.getManager();
        Workspace workspace = (Workspace) appTemplate.getWorkspaceComponent();

        profiledata = null;
        setGameState(GameState.LOGGED_IN_NO_GAME);
        workspace.initHomeScreen();
        createAlert(Alert.AlertType.INFORMATION, propertyManager.getPropertyValue(LOGGED_OUT_TITLE), null, propertyManager.getPropertyValue(LOGGED_OUT_MESSAGE));
    }
}
