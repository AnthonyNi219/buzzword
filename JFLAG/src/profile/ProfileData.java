package profile;

import components.AppDataComponent;
import org.jasypt.util.password.*;

/**
 * Created by Anthony on 11/14/2016.
 */
public abstract class ProfileData implements AppDataComponent {
    private String username;
    private String password;
    private ConfigurablePasswordEncryptor passwordEncryptor;

    public ProfileData() {
        passwordEncryptor = new ConfigurablePasswordEncryptor();
        this.username = "admin";
        this.password = encryptPassword("root");
    }

    public ProfileData(String user, String pass) {
        passwordEncryptor = new ConfigurablePasswordEncryptor();
        this.username = user;
        this.password = encryptPassword(pass);
    }

    public void setUsername(String user) {
        this.username = user;
    }

    public String getUsername() { return username; }

    //Only use setPassword if the password is already encrypted (Reading existing profiles)
    public void setPassword(String pass) {
        this.password = pass;
    }

    public String getPassword() { return password; }

    public String encryptPassword(String pass) {
        passwordEncryptor.setAlgorithm("SHA-512");
        passwordEncryptor.setPlainDigest(false);
        String encryptedPassword = passwordEncryptor.encryptPassword(pass);
        return encryptedPassword;
    }

    public boolean checkPassword(String inputPass) {
        return passwordEncryptor.checkPassword(inputPass, password);
    }

    @Override
    public void reset() {
        this.username = null;
        this.password = null;
    }
}
