package data;

import com.fasterxml.jackson.core.*;
import components.AppDataComponent;
import components.AppFileComponent;
import profile.ProfileData;
import propertymanager.PropertyManager;
import ui.AppMessageDialogSingleton;

import java.io.IOException;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Set;

import static settings.AppPropertyType.*;

import components.AppFileComponent;

/**
 * Created by Anthony on 11/14/2016.
 */
public class BuzzProfileDataFile implements AppFileComponent {

    public static final String USERNAME = "USERNAME";
    public static final String PASSWORD = "PASSWORD";
    public static final String LEVELS = "LEVELS_UNLOCKED";
    public static final String HIGH_SCORES = "HIGH_SCORES";
    public static final String PLAYS = "PLAYS";

    public void saveData(AppDataComponent data, Path to) {
        BuzzProfileData profiledata = (BuzzProfileData) data;
        String username = profiledata.getUsername();
        String password = profiledata.getPassword();
        int[] highScores = profiledata.getHighScores();
        int[] amtPlays = profiledata.getTimesPlayed();
        int[] levelsUnlocked = profiledata.getLevels();

        JsonFactory jsonFactory = new JsonFactory();

        try (OutputStream out = Files.newOutputStream(to)) {

            JsonGenerator generator = jsonFactory.createGenerator(out, JsonEncoding.UTF8);
            generator.useDefaultPrettyPrinter();

            generator.writeStartObject();

            generator.writeStringField(USERNAME, username);
            generator.writeStringField(PASSWORD, password);

            generator.writeFieldName(LEVELS);
            generator.writeStartArray(levelsUnlocked.length);
            for (int i : levelsUnlocked)
                generator.writeString(Integer.toString(i));
            generator.writeEndArray();

            generator.writeFieldName(HIGH_SCORES);
            generator.writeStartArray(highScores.length);
            for (int i : highScores)
                generator.writeString(Integer.toString(i));
            generator.writeEndArray();

            generator.writeFieldName(PLAYS);
            generator.writeStartArray(amtPlays.length);
            for (int i : amtPlays)
                generator.writeString(Integer.toString(i));
            generator.writeEndArray();

            generator.writeEndObject();

            generator.close();

        } catch (IOException e) {
            e.printStackTrace();
            System.exit(1);
        }
    }

    public BuzzProfileData loadData(Path from) throws IOException {
        BuzzProfileData data = new BuzzProfileData();
        data.reset();

        int totalElementsInJson = 0;

        JsonFactory jsonFactory = new JsonFactory();
        JsonParser jsonParser = jsonFactory.createParser(Files.newInputStream(from));

        while(!jsonParser.isClosed()) {
            JsonToken token = jsonParser.nextToken();
            if (JsonToken.FIELD_NAME.equals(token)) {
                totalElementsInJson++;
            }
        }

        if (totalElementsInJson == 5) {
            jsonParser = jsonFactory.createParser(Files.newInputStream(from));

            while(!jsonParser.isClosed()) {
                JsonToken token = jsonParser.nextToken();
                if (JsonToken.FIELD_NAME.equals(token)) {
                    String fieldname = jsonParser.getCurrentName();
                    switch (fieldname) {
                        case USERNAME:
                            jsonParser.nextToken();
                            data.setUsername(jsonParser.getValueAsString());
                            break;
                        case PASSWORD:
                            jsonParser.nextToken();
                            data.setPassword(jsonParser.getValueAsString());
                            break;
                        case LEVELS:
                            int modeLevels = 0;
                            jsonParser.nextToken();
                            while (jsonParser.nextToken() != JsonToken.END_ARRAY) {
                                data.setLevels(modeLevels, jsonParser.getValueAsInt());
                                modeLevels++;
                            }
                            break;
                        case HIGH_SCORES:
                            int modeScore = 0;
                            jsonParser.nextToken();
                            while (jsonParser.nextToken() != JsonToken.END_ARRAY) {
                                data.setHighScores(modeScore, jsonParser.getValueAsInt());
                                modeScore++;
                            }
                            break;
                        case PLAYS:
                            int modePlays = 0;
                            jsonParser.nextToken();
                            while (jsonParser.nextToken() != JsonToken.END_ARRAY) {
                                data.setTimesPlayed(modePlays, jsonParser.getValueAsInt());
                                modePlays++;
                            }
                            break;
                        default:
                            return null;
                    }
                }
            }
        } else {
            return null;
        }
        return data;
    }
}
