package controller;

import java.io.IOException;

/**
 * @author Ritwik Banerjee
 */
public interface FileController {

    void handleNewRequest();

    void handleSaveRequest() throws IOException;

    void handleProfileRequest() throws IOException;

    void handleLoginRequest() throws IOException;

    void handleLogoutRequest();

    void handleExitRequest();
}
