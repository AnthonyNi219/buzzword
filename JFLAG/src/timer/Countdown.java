package timer;

import javafx.animation.KeyFrame;
import javafx.animation.KeyValue;
import javafx.animation.Timeline;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.util.Duration;

import javax.swing.*;

/**
 * Created by Anthony on 12/11/2016.
 */
public class Countdown {
    private static final Integer BASETIME = 60;
    private Timeline timeline;
    private IntegerProperty timeSeconds;
    private Integer defaultTime;
    private boolean started;
    private EventHandler<ActionEvent> onFinished;
    private boolean gameover;

    public Countdown() {
        this.timeSeconds = new SimpleIntegerProperty(BASETIME);
        this.defaultTime = BASETIME;
        this.timeline = new Timeline();
        this.started = false;
        this.onFinished = null;
        this.gameover = false;
    }

    public Countdown(EventHandler<ActionEvent> event) {
        this.timeSeconds = new SimpleIntegerProperty(BASETIME);
        this.defaultTime = BASETIME;
        this.timeline = new Timeline();
        this.started = false;
        this.onFinished = event;
        this.gameover = false;
    }

    public Countdown(Integer time, EventHandler<ActionEvent> event) {
        this.timeSeconds = new SimpleIntegerProperty(time);
        this.defaultTime = time;
        this.timeline = new Timeline();
        this.started = false;
        this.onFinished = event;
        this.gameover = false;
    }

    public IntegerProperty getTimeSeconds() { return timeSeconds; }

    public Timeline getTimeline() { return timeline; }

    public boolean isStarted() { return started; }

    public EventHandler<ActionEvent> getOnFinished() { return onFinished; }

    public boolean isGameover() { return gameover; }

    public void setTimeline(Timeline timeline) {
        this.timeline = timeline;
    }

    public void setTimeSeconds(Integer time) {
        this.timeSeconds = new SimpleIntegerProperty(time);
    }

    public void setStarted(boolean start) { this.started = start; }

    public void setOnFinished(EventHandler<ActionEvent> event) { this.onFinished = event; }

    public void setGameover(boolean gameover) { this.gameover = gameover; }

    public void startTimer() {
        if (timeline != null) {
            timeline.stop();
            setStarted(false);
        }
        if (!gameover) {
            timeSeconds.set(defaultTime);
            timeline = new Timeline();
            timeline.getKeyFrames().add(new KeyFrame(Duration.seconds(defaultTime), onFinished, new KeyValue(timeSeconds, 0)));
            setStarted(true);
            timeline.playFromStart();
        }
    }

    public void pauseTimer() {
        timeline.pause();
    }

    public void resumeTimer() {
        if (!gameover)
            timeline.play();
    }

    public void stopTimer() {
        timeline.stop();
    }
}
