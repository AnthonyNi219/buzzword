package profile;

import java.util.ArrayList;

/**
 * Created by Anthony on 11/14/2016.
 */
public abstract class ProfileList extends ArrayList {

    public abstract boolean containsUser(String user);

    @Override
    public abstract boolean add(Object e);

    @Override
    public abstract void add(int index, Object e);
}
