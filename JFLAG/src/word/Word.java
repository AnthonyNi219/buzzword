package word;

import components.AppDataComponent;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;

/**
 * Created by Anthony on 12/7/2016.
 */
public class Word implements AppDataComponent {
    private SimpleStringProperty word;
    private SimpleIntegerProperty score;

    public Word() {
        this.word = null;
        this.score = null;
    }

    public Word(String word) {
        this.word = new SimpleStringProperty(word);
        setScore(word);
    }

    public Word(String word, int score) {
        this.word = new SimpleStringProperty(word);
        this.score = new SimpleIntegerProperty(score);
    }

    public void setWord(String word) {
        this.word = new SimpleStringProperty(word);
    }

    public void setScore(String word) {
        int wordLength = word.length();
        if (wordLength < 3)
            this.score = new SimpleIntegerProperty(0);
        else
            this.score = new SimpleIntegerProperty((wordLength - 2) * 10);
    }

    public void setScore(int score) {
        this.score = new SimpleIntegerProperty(score);
    }

    public String getWord() { return word.get(); }

    public int getScore() { return score.get(); }

    @Override
    public void reset() {
        this.word = null;
        this.score = null;
    }
}
