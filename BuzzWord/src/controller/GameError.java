package controller;

/**
 * Created by Anthony on 11/14/2016.
 */

public class GameError extends Error {

    public GameError(String message) {
        super(message);
    }
}
