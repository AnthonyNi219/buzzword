package buzzword;

import apptemplate.AppTemplate;
import components.AppComponentsBuilder;
import components.AppDataComponent;
import components.AppFileComponent;
import components.AppWorkspaceComponent;
import data.BuzzProfileData;
import data.BuzzProfileDataFile;
import gui.Workspace;

/**
 * Created by Anthony on 11/14/2016.
 */
public class BuzzWord extends AppTemplate {

    public static void main(String[] args) {
        launch(args);
    }

    public String getFileControllerClass() {
        return "BuzzWordController";
    }

    @Override
    public AppComponentsBuilder makeAppBuilderHook() {
        return new AppComponentsBuilder() {
            @Override
            public AppDataComponent buildDataComponent() throws Exception {
                return new BuzzProfileData();
            }

            @Override
            public AppFileComponent buildFileComponent() throws Exception {
                return new BuzzProfileDataFile();
            }

            @Override
            public AppWorkspaceComponent buildWorkspaceComponent() throws Exception {
                return new Workspace(BuzzWord.this);
            }
        };
    }
}